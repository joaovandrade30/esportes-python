# Exercício

- Durante as olímpiadas existem diversos tipos de esportes que para determinar o vencedor é preciso realizar algum tipo de cálculo, como por exemplo:
  - Arremesso de peso - O/A atleta possui três arremessos. A posição na classificação é determinada pela distânca obtida no maior arremesso válido; em, caso de empate, vale a segunda marca do atleta.
  - Ginástia Artística - O/A atleta recebe cinco notas de diferentes juízes e a nota mais baixa é excluída da somatória final das notas.

## Observações:
- A competição será realizada entre apenas dois adversários;
- É neessário criar um menu com escolhas para as modalidades e as entradas dos dados (arremessos e notas).

### Faça:
- Um programa em Python, utilizando o paradigma orientado a objetos, que determina o vencedor nas duas modalidades apresentadas acima. 
